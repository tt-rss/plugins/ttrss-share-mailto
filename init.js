/* global require, PluginHost, Plugins, Headlines, xhr, dojo, fox, __ */

Plugins.Mailto = {
	init: function() {
		PluginHost.register(PluginHost.HOOK_HEADLINE_TOOLBAR_SELECT_MENU_ITEM2, (action) => {
			if (action == "Plugins.Mailto.send()")
				this.send();

				return true;
		});
	},
	send: function (id) {
		if (!id) {
			const ids = Headlines.getSelected();

			if (ids.length == 0) {
				alert(__("No articles selected."));
				return;
			}

			id = ids.toString();
		}

		const dialog = new fox.SingleUseDialog({
			title: __("Forward article by email (mailto:)"),
			content: __("Loading, please wait...")
		});

		const tmph = dojo.connect(dialog, 'onShow', function () {
			dojo.disconnect(tmph);

			xhr.post("backend.php", App.getPhArgs("mailto", "emailArticle", {ids: id}), (reply) => {
				dialog.attr('content', reply);
			});
		});


		dialog.show();
	}
};

require(['dojo/_base/kernel', 'dojo/ready'], function  (dojo, ready, script) {
	ready(function() {

		// override default hotkey action if enabled
		Plugins.Mail = Plugins.Mail || {};

		Plugins.Mail.onHotkey = function(id) {
			Plugins.Mailto.send(id);
		};

		Plugins.Mailto.init();
	})
});
